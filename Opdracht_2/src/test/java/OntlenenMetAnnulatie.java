import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class OntlenenMetAnnulatie {
    @When("{string} aangeeft dat {string} {string} wil ophalen")
    public void aangeeftDatWilOphalen(String arg0, String arg1, String arg2) {
    }

    @And("{string} aangeeft dat {string} {string} wil annuleren")
    public void aangeeftDatWilAnnuleren(String arg0, String arg1, String arg2) {
    }

    @Then("er wordt een afhalingtransactie aangemaakt")
    public void erWordtEenAfhalingtransactieAangemaakt() {
    }

    @And("er wordt een transactielijn gemaakt voor de ontlening van {string} met een prijs van {int} SP")
    public void erWordtEenTransactielijnGemaaktVoorDeOntleningVanMetEenPrijsVanSP(String arg0, int arg1) {
    }

    @And("er wordt een transactielijn gemaakt voor de waarborg van {string} met een prijs van {int} SP")
    public void erWordtEenTransactielijnGemaaktVoorDeWaarborgVanMetEenPrijsVanSP(String arg0, int arg1) {
    }

    @And("er wordt een transactielijn gemaakt voor annulatie van {string} met een prijs van {int} SP")
    public void erWordtEenTransactielijnGemaaktVoorAnnulatieVanMetEenPrijsVanSP(String arg0, int arg1) {
    }

    @And("heeft {string} {int} SP")
    public void heeftSP(String arg0, int arg1) {
    }
}
