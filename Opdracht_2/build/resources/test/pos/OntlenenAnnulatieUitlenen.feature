Feature: Ontlenen
  Hier komt ook een annulatie bij door uitlener

  Scenario: Ontlening met annulatie door uitlener
    Given today is "15/12/2019"
    When "Eveline" aangeeft dat "Diederik" "Res1" wil ophalen
    And "Eveline" aangeeft dat "Eveline" "Res2" wil annuleren
    Then er wordt een afhalingtransactie aangemaakt
    And er wordt een transactielijn gemaakt voor de ontlening van "Res1" met een prijs van 30 SP
    And er wordt een transactielijn gemaakt voor de waarborg van "Res1" met een prijs van 40 SP
    And er wordt een transactielijn gemaakt voor annulatie van "Res2" met een prijs van -4 SP
    And heeft "Diederik" 934 SP
    And heeft "Eveline" 1066 SP